const express = require('express')
var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')
var routerIndex = require('@/routes/index');
var routerUsers = require('@/routes/users');

//* view engine setup *//

function setViews(app){
   app.set('views', path.join(__dirname, 'views'));
   app.set('view engine', 'pug');

   return app
}

//* MIDDLEWARES */
//* third party Middlewares *//
function useThirds(app){
   app.use(cors())
   app.use(logger('dev'));
   app.use(express.json());
   app.use(express.urlencoded({ extended: false }));
   app.use(cookieParser())
   
   return app
}

function useRoutes(app){
   app.use('/', routerIndex)
   app.use('/users', routerUsers)

   return app
}

function useVendors(app){

   //* Express Vendor Middlewares**/
   app.use(express.static(path.join(__dirname, 'public')));
   
   //* catch 404 and forward to error handler *//
   app.use(function(req, res, next) {
     next(createError(404));
   });
   
   //* error handler *//
   app.use(function(err, req, res, next) {
     // set locals, only providing error in development
     res.locals.message = err.message;
     res.locals.error = req.app.get('env') === 'development' ? err : {};
   
     // render the error page
     res.status(err.status || 500);
     res.render('error');
   })

   return app
}

function useApp(app){
   setViews(app)
   useThirds(app)
   useRoutes(app)
   useVendors(app)
}
module.exports = useApp
