const express = require('express')
var debug = require('debug')('server:server');
var http = require('http');

const setup = require('@/app')

function normalize(val) {
   var num = parseInt(val, 10);
 
   if(isNaN(num)) return val
   if (num >= 0) return num
 
   return false
}
const onError = (port) => (error) => {
   if (error.syscall !== 'listen')
      throw error

   var bind = typeof port === 'string' ?
      'Pipe ' + port :
      'Port ' + port;

   switch (error.code) {
      case 'EACCES':
         console.error(bind + ' requires elevated privileges');
         process.exit(1);
         break;
      case 'EADDRINUSE':
         console.error(bind + ' is already in use');
         process.exit(1);
         break;
      default:
         throw error;
   }
}
const onListening = (server) => () => {
   var addr = server.address();
   var bind = typeof addr === 'string'
     ? 'pipe ' + addr
     : 'port ' + addr.port;
   debug('Listening on ' + bind);
   console.log('Listening on port ' + bind);
}

const start = (app, port= '3000') => {
   port = normalize(port)
   app.set('port', port)
   var server = http.createServer(app);
   
   server.on('error', onError(port));
   server.on('listening', onListening(server));

   server.listen(port)
}
const server= {
   app: express(),
   get port(){ process.env.PORT || '3000'},
   set port(port){
         port = normalize(port)
         this.app.set('port', port)
   },
   setup: function(){
      setup(this.app)
   },
   start: function(){ 
      start(this.app, this.port)
   }
}

module.exports = server